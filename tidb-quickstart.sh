#!/usr/bin/env bash
echo "请确定docker已经安装,参考连接为 http://gitlab.java.yibainetwork.com/Pass/docker-install"
echo "请确定docker-compose已经安装,参考链接为 http://gitlab.java.yibainetwork.com/Pass/docker-compose-install"
echo "请确定git已经安装"


git clone https://github.com/pingcap/tidb-docker-compose.git
cd tidb-docker-compose
docker-compose pull
docker-compose up -d